
public class konsolenausgabe 
{
		public static void main(String[] args) 
		{
			System.out.println("Aufgabe 1");
			System.out.print("Das ist der erste Beispielsatz. ");
			System.out.print("Das ist der zweite Beispielsatz \n");
			System.out.println(" ");
			
			System.out.println("Das ist der erste 'Beispielsatz'\n" + "Das ist der Zweite Beispielsatz"); 
			System.out.println(" ");
			
		

			System.out.println("Aufgabe 2");
			System.out.printf("%10s", "*");	
			System.out.printf("%-10s \n", "*");
			System.out.printf("%10s", "**");
			System.out.printf("%-10s \n", "**");
			System.out.printf("%10s", "***");
			System.out.printf("%-10s \n", "***");
			System.out.printf("%10s", "****");
			System.out.printf("%-10s \n", "****");
			System.out.printf("%10s", "**");
			System.out.printf("%-10s \n", "**");
			System.out.printf("%10s", "**");
			System.out.printf("%-10s \n", "**");
			System.out.println("\n");
			
			System.out.println("Aufgabe 3");
			double a = 22.4234234;
			System.out.printf("%.2f \n", a);
			double b = 111.2222;
			System.out.printf("%.2f \n", b);
			double c = 4;
			System.out.printf("%.2f \n", c);
			double d = 1000000.551;
			System.out.printf("%.2f \n", d);
			double e = 97.34;
			System.out.printf("%.2f \n", e);
			System.out.println(" ");
			
			System.out.println("�2 Aufgabe 1");
			System.out.printf("%-5s = %-18s = %4d\n", "0!", "", 1);
			System.out.printf("%-5s = %-18s = %4d\n", "1!", "1", 1);
			System.out.printf("%-5s = %-18s = %4d\n", "2!", "1 * 2", 1 * 2);
			System.out.printf("%-5s = %-18s = %4d\n", "3!", "1 * 2 * 3", 1 * 2 * 3);
			System.out.printf("%-5s = %-18s = %4d\n", "4!", "1 * 2 * 3 * 4", 1 * 2 * 3 * 4);
			System.out.printf("%-5s = %-18s = %4d\n", "5!", "1 * 2 * 3 * 4 * 5", 1 * 2 * 3 * 4 * 5);
			
			
			System.out.println("");
			
			System.out.println("�2 Aufgabe 3");
			System.out.printf("%-11s|%9s\n", "Fahrenheit", "Celsius");
			System.out.printf("%20s\n", "---------------------");
			System.out.printf("%-11d|  %.2f\n", -20, -28.8889);
			System.out.printf("%-11d|  %.2f\n", -10, -23.3333);
			System.out.printf("%-11d|  %.2f\n", 0, -17.7778);
			System.out.printf("%-11d|   %.2f\n", 20, -6.6667);
			System.out.printf("%-11d|   %.2f\n", 30, -1.1111);
			
		}

	}