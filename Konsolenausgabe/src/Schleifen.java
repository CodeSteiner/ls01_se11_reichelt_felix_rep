public class Schleifen {
	
	public static void main(String[] Args)
	{
		int i =1;
		
		for (i=1; i<= 200; i++)
		{
			if( i % 3 == 0&& i%5 == 0)
			{
				System.out.println(i+ " "	+ "foobar");
			}
			else if(i%3 == 0)
			{
				System.out.println(i+ " foo");
			}
			else if ( i %5 == 0 )
			{
				System.out.println(i+ " bar");
			}
			else
			{
				System.out.println(i);
			}
		}
		i--;
		
		while(i>= 1)
		{
			if( i % 3 == 0&& i%5 == 0)
			{
				System.out.println(i+ " "	+ "foobar");
			}
			else if(i%3 == 0)
			{
				System.out.println(i+ " foo");
			}
			else if ( i %5 == 0 )
			{
				System.out.println(i+ " bar");
			}
			else
			{
				System.out.println(i);
			}
			i--;
		}
		i=1;
		do
		{
			
			if( i % 3 == 0&& i%5 == 0)
			{
				System.out.println(i+ " "	+ "foobar");
			}
			else if(i%3 == 0)
			{
				System.out.println(i+ " foo");
			}
			else if ( i %5 == 0 )
			{
				System.out.println(i+ " bar");
			}
			else
			{
				System.out.println(i);
			}
			i++;
		}while(i<=200);
		for (; i>= 1; i-=2)
		{
			if( i % 3 == 0&& i%5 == 0)
			{
				System.out.println(i+ " "	+ "foobar");
			}
			else if(i%3 == 0)
			{
				System.out.println(i+ " foo");
			}
			else if ( i %5 == 0 )
			{
				System.out.println(i+ " bar");
			}
			else
			{
				System.out.println(i);
			}
		}
	}	
}
